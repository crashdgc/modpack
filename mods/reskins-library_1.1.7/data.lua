-- Copyright (c) 2021 Kirazy
-- Part of Artisanal Reskins: Library
--
-- See LICENSE.md in the project directory for license information.

-- Core functions
require("prototypes.functions.functions")
require("prototypes.functions.tints")
require("prototypes.functions.entity-rescaling")
require("prototypes.functions.icon-handling")
require("prototypes.functions.pipe-pictures")
require("prototypes.functions.label-items")

----------------------------------------------------------------------------------------------------
-- ENTITIES
----------------------------------------------------------------------------------------------------
-- Base
require("prototypes.entity.base.oil-refinery")
require("prototypes.entity.base.splitter")
require("prototypes.entity.base.transport-belt")
require("prototypes.entity.base.underground-belt")

----------------------------------------------------------------------------------------------------
-- TECHNOLOGIES
----------------------------------------------------------------------------------------------------
-- Base
require("prototypes.technology.base.logistics")