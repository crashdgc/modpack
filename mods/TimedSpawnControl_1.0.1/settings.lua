data:extend{
    {
		type = "int-setting",
		name = "respawn-timer",
        setting_type = "runtime-global",
		default_value = 60,
		minimum_value = 0,
	},
}
