data:extend({
        {
            type = "bool-setting",
            name = "unminableConstruction",
            setting_type = "startup",
            default_value = true,
        },
        {
            type = "bool-setting",
            name = "unminableLogistic",
            setting_type = "startup",
            default_value = true,
        },
    })