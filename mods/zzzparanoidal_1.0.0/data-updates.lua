require("prototypes.miniloaders")
require("prototypes.generators")

if mods.bobequipment then
	data.raw.item["personal-roboport-mk3-equipment"].subgroup = "misc1"
	data.raw.item["personal-roboport-mk4-equipment"].subgroup = "misc1"
end
