<table align="center"><tr><td align="center" width="9999">
<!-- ![ICON](https://cdn.discordapp.com/icons/569536773701500928/8df51b553f826280000ce8f7d1fc7f45.png?size=128) -->
![LOGO](https://i107.fastpic.ru/big/2018/1219/42/82dca31e722dda962a4550df5138bb42.png?r=1)

для Factorio 1.1.35 (протестировано) и выше


------------
**PARANOIDAL** - довольно хардкорная и очень интересная мегасборка для **Factorio**.  
Здесь внедрено порядка 180 отдельных модов, тесно и нетесно переплетённых между собой в удивительную игру.

> Ванильная Factorio по сравнению с этим - безалкогольная водка.
</td></tr>
<tr><td align="center" width="9999">

<a href="https://crowdin.com/project/factorio-paranoidal?utm_source=badge&utm_medium=referral&utm_campaign=badge-add-on" rel="nofollow"><img style="width:140;height:40px" src="https://badges.crowdin.net/badge/light/crowdin-on-dark.png" srcset="https://badges.crowdin.net/badge/light/crowdin-on-dark.png 1x,https://badges.crowdin.net/badge/light/crowdin-on-dark@2x.png 2x" alt="Crowdin | Agile localization for tech companies" /></a>

</td></tr></table>

## Ссылки
- [Этот репозиторий](https://gitlab.com/paranoidal/modpack)
- [Discord сервер](https://discord.com/invite/AWStgXd) - информация для доступа к официальному серверу Factorio, с данной сборкой
- [Раздача на Rutracker](https://rutracker.org/forum/viewtopic.php?t=5612345) - сборка + Repack игры

## Данная сборка вам подойдет, если:
 - У вас ОЧЕНЬ много **свободного времени** и вам абсолютно некуда его больше тратить
 - Вам очень нравится играть в **Factorio**, разрабатывать новые производственные цепочки, оптимизировать производство и хочется преодолеть новый невероятный челлендж
 - Вы полагаете, что в оригинальной игре **слишком мало контента** и она достойна большего
 - Вы уже **прошли оригинал** и сборку модов Bob's, легко высылаете в космос несколько ракет в минуту и ищите чего-то нового и зажигательного
 - Вам **не удалось собрать** собственную мега-сборку модов, а то, что есть в доступе, вас не устраивает
 - Вас **не пугают**, а скорее интересуют вещи, созданные личностями с паранояльными диагнозами
 - Вы испытываете **глубокое удовлетворение** перфекциониста, когда вы создаёте и завершаете гигантское многокомпонентное производство, постоянно улучшаете его производительность, лаконичность внутреннего устройства, доводя все действующие компоненты до абсолютного совершенства - как раз в этой сборке вы сможете наслаждаться этим действительно бесконечно!

## В сборке вы найдете:
 - С нуля **переведённые моды**, а также практически полная ревизия + ручной допил всего имевшегося перевода модов; очень много правок, достигалась цель унификации разных переводов;
 - **Многочисленные доработки**, ребаланс, ручные твики и глубокая адаптация друг к другу имеющихся модов; скорее всего, *баланс будет изменён в обновлениях сборки, но это не точно*;
 - Тщательно выбранная из множества, либо своя **эксклюзивная графика** некоторых компонентов/элементов; Но здесь отсутствуют всякие моды на супер-HD поезда и т.д. Их вы можете прикрутить самостоятельно *(если позволит компьютер и если ваши моды не забагуют всю сборку)*;
 - **Злобные жуки!** И ещё более злобное оружие для их уничтожения!
 - **Параноидальное количество производственных цепочек**, рецептов, пронизывающие всё научное дерево схемы получения веществ, технологий и ещё х.з. чего;
 - **Невменяемые цены** для постройки заводов, энергетические требования на науку и даже на простые компоненты - всё очень дорого! Но ничего, вы обязательно найдёте разные бесконечные источники ресурсов, энергоносителей и вам на всё хватит. Со временем...
 - Запуск первой ракеты не завершает игру! Это лишь начало для создания целой орбитальной сети спутников, постройки судоверфи в космосе и **межзвёздного космического корабля!**
 - Убойное марафонское и действительно **параноидальное время на прохождение**, т.е. вам обязательно \_не\_хватит\_времени поиграть в это.

## Как начать

- Установщик
  1. Скачать раздачу с [rutracker](https://rutracker.org/forum/viewtopic.php?t=5612345)
  2. Установить через скачаный установщик
  3. Играть
  - *В раздаче на rutracker'e, вполне возможно не самая последняя версия сборки. Если вам нужны последние изменения то для вас следующий путь*
- Ручной
  1. Установить Factorio ***1.1.30***. [Скачать с официального сайта](https://factorio.com/download/archive)
  2. Склонировать репозиторий (или скачать, кнопка Download вверху)
  3. Удалить все уже установленные моды
  3. Поместить содержимое скачанной папки `mods` в папку `mods` для Factorio
  4. Играть

## Дополнительная информация
<details> 
  <summary>Описание</summary>
   Не спортсмен, не солдат, а простой дегенерат: потратил на игру более 3000 часов, но не напрасно! Перед вами всем модам мод - Factorio PARANOIDAL. Здесь внедрено порядка 160 отдельных модов, тесно и нетесно переплетённых между собой в удивительную игру.
Вообще, изначально мод собирался под себя и для друзей, но вот был выложен и в общий доступ. Надеюсь, что те, кому не понравится,- сами доведут до ума раздражающие их элементы без нытья. В любом случае, угодить всем не получится - ибо велик был мудрец, огорчившийся, когда его творение понравилось многим. Успехов!
</details>
<details> 
  <summary>Каноничный отзыв</summary>
   Corvins писал(а):  

   > "У нас была установленная Factorio, 75 часов свободного времени, сборка Bob's Mod, Angel's Ore и целое множество модов всех категорий и направленностей. Авиа-производство и робототехника, аватары, отдаленный спавн ресурсов и дюжина сборок с новыми технологиями и плюшками. Не то, чтобы это был необходимый комплект для игры, но, если начал устанавливать моды, становится сложно остановиться. Единственное, что вызывало у меня опасение - это био-индустрия. Ничто не меняет баланс так кардинально, как добыча нефти из песка. Я знал, что рано или поздно мы перейдем и на эту дрянь."
</details>
<details> 
  <summary>Список модов</summary>
  
- aai-industry_9.2.4  

- AbandonedRuins_0.2.9  

- Aircraft_1.7.1  

- angelsbioprocessing_0.7.13  

- angelsinfiniteores_0.9.4  

- angelspetrochem_0.9.13  

- angelsrefining_0.11.15  

- angelssmelting_0.6.10  

- AtomicArtillery_0.1.12  

- beautiful_straight_bridge_railway_0.18.0  

- BigLab_9.0.2  

- Bio_Industries_9.17.1  

- Bio_Industries_9.18.25  

- blueprint_flip_and_turn_18.7.0  

- bobassembly_0.18.7  

- bobelectronics_0.18.1  

- bobenemies_0.18.5  

- bobequipment_0.18.1  

- bobicons_0.18.3  

- bobinserters_0.18.4  

- boblibrary_0.18.10  

- boblogistics_0.18.9  

- bobmining_0.18.3  

- bobmodules_0.18.5  

- bobores_0.18.3  

- bobplates_0.18.9  

- bobpower_0.18.7  

- bobrevamp_0.18.6  

- bobtech_0.18.3  

- bobvehicleequipment_0.18.1  

- bobwarfare_0.18.6  

- Bottleneck_0.11.4  

- bullet-trails_0.5.1  

- BurnerOffshorePump_9.1.5  

- chromatic-belts_3.1.4  

- Clowns-AngelBob-Nuclear_1.1.15  

- Clowns-Nuclear_1.3.10  

- Clowns-Processing_1.3.12  

- ColorTIERHR_9.17.6  

- ColorTIERHR_9.17.61  

- comb_0.1.2  

- CoppermineBobModuleRebalancing_0.3.1  

- CopyPasteModules_0.0.3  

- Cursed-FMD_0.1.2  

- CW-carbon-capture-reforged_9.1.3  

- DeadlockLargerLamp_1.2.4  

- DeadlockLargerLamp_1.3.1  

- DeleteEmptyChunks_0.4.2  

- DewPointAggregator_9.0.28  

- efficient-research_0.0.3  

- Enhanced_Map_Colors_1.5.3  

- ERPTbaAB_0.2.2  

- even-distribution_0.3.18  

- EvoGUI_0.4.501  

- expanded-rocket-payloads_0.17.1  

- extendedangels_0.3.12  

- Factorissimo2_2.4.2  

- FARL_4.0.2  

- FastRemoveTiles_0.0.9  

- Flammable_Oils_fix_9.2.2  

- flib_0.3.0  

- Flow Control_3.0.6  

- FluidMustFlow_1.2.4  

- FluidMustFlow_1.2.8  

- FluidWagonColorMask_1.0.1  

- FNEI_0.3.4  

- helmod_0.10.25  

- Honk_4.1.0  

- InfiniteTech_0.5.3  

- InlaidLampsExtended_0.1.8  

- JunkTrain3_0.18.1  

- KaoExtended_9.16.12  

- KS_Power_9.3.7  

- laser_fix_0.18.12  

- LightedPolesPlus_1.5.10  

- lightorio_0.18.5  

- LogisticTrainNetwork_1.13.10  

- LtnManager_0.2.7  

- LTN_Combinator_0.6.2  

- LTN_Content_Reader_0.3.4  

- make_burner_miners_great_again_9.1.0  

- marathon_9.1.17  

- MilesBobsExpansion_9.6.1  

- miniloader_1.11.3  

- minime_0.0.18  

- mod-list.json  

- mod-settings.dat  

- more-minimap-autohide-017_1.0.1  

- more-petrochem-hell_0.18.2  

- MoreAchievements_0.5.1  

- multi-product-recipe-details_0.18.1  

- MultipleUnitTrainControl_0.2.4  

- MxlChievements_1.1.8  

- Nanobots_3.2.8  

- Natural_Evolution_Enemies_0.17.19  

- Natural_Evolution_Enemies_0.18.05  

- ner_intermediatestweak_1.0.0  

- NightBrightness_0.0.6  

- Noxys_Trees_0.2.2  

- nuke-cliffs_18.0.0  

- Oberhaul_9.16.22  

- OberNuclear_0.17.0  

- Orbital Ion Cannon_1.8.2  

- original-music-hd-updated_0.17.0  

- OverloadedTrains_0.18.4  

- paranoidal-tweaks_0.18.34  

- PCPRedux_0.18.5  

- PickerAtheneum_1.2.2  

- PickerBlueprinter_1.1.3  

- PickerEquipmentKeys_1.1.1  

- PickerExtended_4.1.2  

- PickerInventoryTools_1.1.6  

- PickerPipeTools_1.1.1  

- PickerTweaks_2.2.2  

- PickerVehicles_1.1.2  

- Picks-Inserter_1.18.1  

- platforms_19.18.3  

- Pollution_Control_1.0.7  

- qol_research_3.1.1  

- railloader_1.0.5  

- RaiLuaLib_0.2.8  

- Rampant_0.17.26  

- Rampant_0.18.17  

- RealisticDecorationCleanup_1.0.0  

- Realistic_Electric_Trains_9.4.5  

- Renamer_2.1.6  

- research-progress_0.18.0  

- research_causes_evolution_0.17.2  

- reskins-bobs_0.0.22  

- reskins-library_0.0.15  

- ReStack_0.6.3  

- Robot256Lib_0.18.7  

- rso-mod_6.1.0  

- RU-locale_9.18.0  

- Sandros-fixes_0.5.5  

- scattergun_turret_5.3.2  

- SchallPickupTower_0.18.2  

- SchallTankPlatoon_0.18.6  

- Shield-FX_0.18.4  

- ShinyAngelGFX_9.16.8  

- ShinyBobGFX_9.17.90  

- ShinyBob_Techs_9.16.0  

- Shortcuts-ick_0.18.1  

- SingleColorTerrain_9.0.5  

- Skip Burner Stage_9.16.0  

- SmogSolarPanels_0.2.2  

- SmogVisualPollution_0.2.0  

- SpaceMod_9.3.8  

- SpaceXGAR_0.17.0  

- Squeak Through_1.8.0  

- stdlib_1.4.3  

- Subterranean_0.5.3  

- Texugo_windgenerator_9.17.0  

- toxicPollution_0.3.6  

- TrainOverhaul_0.3.7  

- Turret-Shields_0.18.41  

- UnminableLogisticBots_1.0.2  

- upgrade-planner-next_2.1.2  

- Warehousing_9.2.1  

- weaponSoundsRedone_1.3.0  

- what-fish_0.1.1  

- what-is-it-really-used-for_1.5.13  

- WideChests_3.0.7  

- ZCS-Trash-Landfill-Continued-Continued_1.0.0  

- zero-fluid-info_0.0.3  

- zzzparanoidal_0.18.30  

</details>
